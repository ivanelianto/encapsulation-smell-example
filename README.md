# Tugas GSLC Code Reenginering 11 Mei 2019
### oleh Ivan Favian Elianto (2101667595)

### Content:
- Contoh ```Missing Encapsulation```
- Contoh ```Unexploited Encapsulation```

#### Contoh *Missing Encapsulation* :  
- Ubah Branch Menjadi ```missing-encapsulation-before``` Untuk Melihat Contoh Kasus
- Ubah Branch Menjadi ```missing-encapsulation-after``` Untuk Melihat Solusi

#### Contoh *Unexploited Encapsulation* :  
- Ubah Branch Menjadi ```unexploited-encapsulation-before``` Untuk Melihat Contoh Kasus
- Ubah Branch Menjadi ```unexploited-encapsulation-after``` Untuk Melihat Solusi
  

>    
> #### **Tidak Mengerti Apa Itu Github?**
> Silahkan Pelajari Lebih Lanjut Di [sini](https://guides.github.com/activities/hello-world/)